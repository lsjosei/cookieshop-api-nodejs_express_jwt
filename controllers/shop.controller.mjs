import { Product } from "../models/product.mjs"
import { Shop } from "../models/shop.mjs"

const getAllShops = async (req, res, next) => {
  try {
    //get all shops
    const dbShops = await Shop.find()
    return res.status(200).json(dbShops)
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: err,
    })
  }
}

const getShopById = async (req, res, next) => {
  try {
    //get a shop by its ID
    const { shopId } = req.params

    const dbShop = await Shop.findById(shopId, {
      _id: 0,
      name: 1,
      address: 1,
      products: 1,
    })
    if (dbShop) return res.status(200).json(dbShop)
    return res.status(404).json({
      status: 404,
      message: "can't find shop",
      data: {},
    })
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: err,
    })
  }
}

const createShop = async (req, res, next) => {
  try {
    //create a new shop
    const shopParams = { ...req.body }

    if (!shopParams.name || !shopParams.address) {
      return res.status(400).json({
        status: 400,
        message: "missing parameters",
        data: {},
      })
    }
    shopParams.products = {}

    const newShop = new Shop(shopParams)

    const insertion = await newShop.save()

    res.status(200).json({
      status: 200,
      message: "shop created",
      data: insertion,
    })
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: err,
    })
  }
}

const deleteShop = async (req, res, next) => {
  try {
    //delete a shop
    const { shopId } = req.params

    const deletedShop = await Shop.findByIdAndDelete(shopId)

    if (deletedShop) {
      return res.status(200).json({
        status: 200,
        message: "shop deleted",
        data: deletedShop,
      })
    }
    return res.status(404).json({
      status: 404,
      message: "cannot find shop",
      data: {},
    })
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: err,
    })
  }
}

const modifyShop = async (req, res, next) => {
  try {
    //modify a shop
    const { shopId } = req.params
    const shopParams = { ...req.body }

    if (shopId) {
      const newShop = await new Shop(shopParams)

      newShop._id = shopId

      const updatedShop = await Shop.findByIdAndUpdate(shopId, newShop, {
        overwrite: true,
      })

      if (updatedShop) {
        const shop = await Shop.findById(shopId)
        return res.status(200).json({
          status: 200,
          message: "Shop updated",
          data: shop,
        })
      }
      return res.status(404).json({
        status: 404,
        message: "Shop not found",
        data: {},
      })
    }

    return res.status(400).json({
      status: 400,
      message: "Missing shop id",
      data: {},
    })
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: err,
    })
  }
}

const addProductToShop = async (req, res, next) => {
  try {
    //add a product to a shop
    const { shopId } = req.params

    const { productName, quantity } = req.body

    const product = await Product.findOne({ name: productName })

    //compruebo que el producto es valido
    if (!product)
      return res.status(404).json({
        status: 404,
        message: "product not found",
      })

    const foundShop = await Shop.findById(shopId)

    //compruebo que la tienda es valida
    if (!foundShop)
      return res.status(404).json({
        status: 404,
        message: "shop not found",
      })

    //añado el prodcuto al dict de productos de la tienda
    const products = { ...foundShop.products }

    if (product._id in products) products[product._id] += quantity
    else products[product._id] = quantity
    await Shop.findByIdAndUpdate(shopId, { products })

    //recojo la tienda modificada y respondo
    const modifiedShop = await Shop.findById(shopId)
    return res.status(200).json({
      status: 200,
      message: "product added",
      data: modifiedShop,
    })
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: err,
    })
  }
}

const deleteProductFromShop = async (req, res, next) => {
  try {
    //delete a product from a shop
    const { shopId } = req.params

    const { productName } = req.body

    const product = await Product.findOne({ name: productName })

    //compruebo que el producto es valido
    if (!product)
      return res.status(404).json({
        status: 404,
        message: "product not found",
      })

    const foundShop = await Shop.findById(shopId)

    //compruebo que la tienda es valida
    if (!foundShop)
      return res.status(404).json({
        status: 404,
        message: "shop not found",
        data: {},
      })

    const products = { ...foundShop.products }

    if (product._id in products) {
      delete products[product._id]

      await Shop.findByIdAndUpdate(shopId, { products })

      //recojo la tienda modificada y respondo
      const modifiedShop = await Shop.findById(shopId)
      return res.status(200).json({
        status: 200,
        message: "product deleted",
        data: modifiedShop,
      })
    }

    return res.status(400).json({
      status: 400,
      message: "product not found in shop",
      data: foundShop,
    })
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: err,
    })
  }
}

const sellProduct = async (req, res, next) => {
  try {
    //reduces the number of available products in a shop
    const { shopId } = req.params
    const { productName, quantity } = req.body

    const product = await Product.findOne({ name: productName })

    const shop = await Shop.findById(shopId)

    const productInventory = shop.products

    if (!product || !(product._id in productInventory)) {
      return res.status(404).json({
        status: 404,
        message: `${productName} not available here`,
        data: {},
      })
    } else if (productInventory[product._id] < quantity) {
      return res.status(400).json({
        status: 400,
        message: `Not enough ${productName}, currently ${
          productInventory[product._id]
        }`,
        data: {},
      })
    }

    productInventory[product._id] -= quantity

    await Shop.findByIdAndUpdate(shopId, { products: productInventory })

    res.status(200).json({
      status: 200,
      message: `${quantity} of ${productName} sold. ${
        productInventory[product._id]
      } left`,
    })
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: err,
    })
  }
}

export {
  getAllShops,
  getShopById,
  createShop,
  deleteShop,
  modifyShop,
  addProductToShop,
  deleteProductFromShop,
  sellProduct
}
