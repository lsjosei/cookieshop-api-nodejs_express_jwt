import bcrypt from "bcrypt"
import jwt from "jsonwebtoken"

import { User } from "../models/user.mjs"

const login = async (req, res, next) => {
  try {
    const { username, password } = req.body

    const user = await User.findOne({ username })

    const isValidPassword = await bcrypt.compare(password, user?.password ?? "")

    if (!user || !isValidPassword) {
      return res.status(401).json({
        status: 401,
        message: "Login error",
      })
    }

    const token = jwt.sign(
      {
        id: user._id,
        username: user.username,
        rol: "ADMIN",
      },
      req.app.get("secretKey"),
      { expiresIn: "1h" }
    )

    return res.status(200).json({
      status: 200,
      message: "Login success!",
      data: {
        id: user._id,
        token: token,
      },
    })
  } catch (err) {
    console.log(err)
    return res.status(500).json({
      status: 500,
      message: err,
    })
  }
}

const register = async (req, res, next) => {
  try {
    const { username, password } = req.body

    //TODO validar la password

    const previousUser = await User.findOne({ username })

    if (previousUser) {
      return res.status(400).json({
        status: 400,
        message: "register error",
      })
    }

    const pwdHash = await bcrypt.hash(password, 10)

    const newUser = await User({
      username,
      password: pwdHash,
    })

    const savedUser = await newUser.save()

    return res.status(200).json({
      status: 200,
      message: "User registered",
      data: {
        id: savedUser._id,
      },
    })
  } catch (err) {
    return res.status(500).json({
      status: 500,
      message: err,
    })
  }
}

const logout = async (req, res, next) => {
  try {
    req.authority = null;
    return res.json({
      status: 200,
      message: 'Logout!',
      token: null
    });
  } catch (error) {
    next(error)
  }
}

export { login, register, logout }
