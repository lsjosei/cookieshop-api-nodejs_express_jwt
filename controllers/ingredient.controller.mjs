import { Product } from "../models/product.mjs"
import { Ingredient } from "../models/ingredient.mjs"

const getAllIngredients = async (req, res, next) => {
  try {
    const ingredients = await Ingredient.find()
    return res.status(200).json(ingredients)
  } catch (err) {
    console.log(err)
    return res.status(500).json({
      status: 500,
      message: err,
    })
  }
}

const getIngredientById = async (req, res, next) => {
  try {
    const { ingredientId } = req.params

    const ingredient = await Ingredient.findById(ingredientId)

    if (ingredient) {
      return res.status(200).json(ingredient)
    }
    return res.status(404).json({
      status: 404,
      message: "ingredient not found",
    })
  } catch (err) {
    console.log(err)
    return res.status(500).json({
      status: 500,
      message: err,
    })
  }
}

const createIngredient = async (req, res, next) => {
  try {
    const { name, isVegan, isVegetarian, isGlutenFree, isLactoseFree } =
      req.body

    const previousIngredient = await Ingredient.findOne({ name })

    if (previousIngredient) {
      return res.status(400).json({
        status: 400,
        message: "ingredient already exists",
        data: previousIngredient,
      })
    }

    const newIngredient = await new Ingredient({
      name,
      isVegan,
      isVegetarian,
      isGlutenFree,
      isLactoseFree,
    })

    const savedIngredient = await newIngredient.save()

    return res.status(200).json({
      status: 200,
      message: "ingrediend created",
      data: savedIngredient,
    })
  } catch (err) {
    console.log(err)
    return res.status(500).json({
      status: 500,
      message: err,
    })
  }
}

const modifyIngredient = async (req, res, next) => {
  try {
    const { ingredientId } = req.params
    const { name, isVegan, isVegetarian, isGlutenFree, isLactoseFree } =
      req.body

    const modifiedIngredient = await Ingredient.findByIdAndUpdate(
      ingredientId,
      { name, isVegan, isVegetarian, isGlutenFree, isLactoseFree },
      { overwrite: true }
    )

    if (modifiedIngredient) {
      const ingredient = await Ingredient.findById(ingredientId)
      return res.status(200).json({
        status: 200,
        message: "ingredient modified",
        data: ingredient,
      })
    }
    return res.status(404).json({
      status: 404,
      message: "ingredient not found",
      data: {},
    })
  } catch (err) {
    console.log(err)
    return res.status(500).json({
      status: 500,
      message: err,
    })
  }
}

const deleteIngredient = async (req, res, next) => {
  try {
    const { ingredientId } = req.params

    const products = await Product.find({ ingredients: ingredientId }).populate(
      "ingredients",
      { name: 1 }
    )
    console.log(products)

    if (products.length === 0) {
      const deletedIngredient = await Ingredient.findByIdAndDelete(ingredientId)

      if (deletedIngredient) {
        return res.status(200).json({
          status: 200,
          message: "ingredient deleted",
          data: deletedIngredient,
        })
      }
      return res.status(404).json({
        status: 404,
        message: "ingredient not found",
      })
    }
    return res.status(400).json({
      status: 400,
      message: "ingredient is part of a product, delete the product first",
      data: products,
    })
  } catch (err) {
    console.log(err)
    return res.status(500).json({
      status: 500,
      message: err,
    })
  }
}

export {
  getAllIngredients,
  getIngredientById,
  createIngredient,
  modifyIngredient,
  deleteIngredient,
}
