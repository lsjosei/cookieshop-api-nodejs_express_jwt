import { Product } from "../models/product.mjs"
import { Ingredient } from "../models/ingredient.mjs"
import { Shop } from "../models/shop.mjs"

const getAllProducts = async (req, res, next) => {
  try {
    //get all products
    const products = await Product.find().populate("ingredients", {
      _id: 1,
      name: 1,
    })

    return res.status(200).json(products)
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: err,
    })
  }
}

const getProductById = async (req, res, next) => {
  try {
    //get one product by ID
    const { productId } = req.params

    const product = await Product.findById(productId, {
      _id: 0,
      name: 1,
      price: 1,
      ingredients: 1,
    }).populate("ingredients", { _id: 1, name: 1 })

    if (product) return res.status(200).json(product)
    return res.status(404).json({
      status: 404,
      message: "product not found",
      data: {},
    })
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: err,
    })
  }
}

const createProduct = async (req, res, next) => {
  try {
    const { name, price, ingredients } = req.body

    if (!name || !price || !ingredients)
      return res.status(400).json({
        status: 400,
        message: "Missing parameters",
        data: {},
      })

    let ingredientsIds = []

    for (let i = 0; i < ingredients.length; i++) {
      let ingredient = await Ingredient.findOne({ name: ingredients[i] })
      if (!ingredient) {
        const newIngredient = await new Ingredient({ name: ingredients[i] })
        ingredient = await newIngredient.save()
      }
      ingredientsIds.push(ingredient._id)
    }

    const product = await new Product({
      name,
      price,
      ingredients: ingredientsIds,
    })
    product.save()

    return res.status(200).json({
      status: 200,
      message: "Product created",
      data: product,
    })
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: err,
    })
  }
}

const deleteProduct = async (req, res, next) => {
  try {
    const { productId } = req.params

    let product = await Product.findById(productId)

    if (product) {
      //falta eliminar el producto de las tiendas
      const shopsProducts = await Shop.find({}, { _id: 0, products: 1 })
      const isInShop = shopsProducts.some((ele) => {
        const dict = ele.products ?? {}
        return product._id in dict
      })
      if (!isInShop) {
        product = await Product.findByIdAndDelete(productId)
        return res.status(200).json({
          status: 200,
          message: "product deleted",
          data: product,
        })
      }
      return res.status(400).json({
        status: 400,
        message: "product available in shop, remove it first from stocks",
        data: {},
      })
    }

    return res.status(404).json({
      status: 404,
      message: "product not found",
      data: {},
    })
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: err,
    })
  }
}

const updateProduct = async (req, res, next) => {
  try {
    const { productId } = req.params
    const { name, price, ingredients } = req.body

    if (!name && !price && !ingredients) {
      return res.status(400).json({
        status: 400,
        message: "Missing parameters",
      })
    }

    let ingredientsIds = []

    for (let i = 0; i < ingredients.length; i++) {
      let ingredient = await Ingredient.findOne({ name: ingredients[i] })
      if (!ingredient) {
        const newIngredient = await new Ingredient({ name: ingredients[i] })
        ingredient = await newIngredient.save()
      }
      ingredientsIds.push(ingredient._id)
    }

    const product = await Product.findByIdAndUpdate(productId, {
      $set: { name, price, ingredientsIds },
    })

    if (!product)
      return res.status(404).json({
        status: 404,
        message: "product not found",
      })

    const updatedProduct = await Product.findById(productId)
    return res.status(200).json({
      status: 200,
      message: "Product updated",
      data: updatedProduct,
    })
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: err,
    })
  }
}

export {
  getAllProducts,
  getProductById,
  createProduct,
  deleteProduct,
  updateProduct,
}
