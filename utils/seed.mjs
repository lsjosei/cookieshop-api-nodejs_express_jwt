import mongoose from "mongoose"
import bcrypt from "bcrypt"

import { Shop } from "../models/shop.mjs"
import { Product } from "../models/product.mjs"
import { User } from "../models/user.mjs"
import { Ingredient } from "../models/ingredient.mjs"

import { connect } from "./db.mjs"

//admin user

const userList = [
  { username: "admin", password: await bcrypt.hash("admin", 10) },
]

const shopList = [
  {
    name: "La pequeña galleta",
    address: "Calle del azucar 3",
    maxEmployeeNumber: 5,
  },
  {
    name: "Dulces sueños",
    address: "Avenida del chocolate 14",
    maxEmployeeNumber: 7,
  },
  {
    name: "Mazapan",
    address: "Calle principal 1",
    maxEmployeeNumber: 3,
  },
]

const productList = [
  {
    name: "Galleta",
    price: 5,
  },
  {
    name: "Tarta de chocolate",
    price: 7,
  },
  {
    name: "Batido de vainilla",
    price: 3,
  },
  {
    name: "Muffin de arándanos",
    price: 4,
  },
]

const ingredientList = [
  {
    name: "Azucar",
    isVegan: true,
    isVegetarian: true,
  },
  {
    name: "Harina",
    isVegan: true,
    isVegetarian: true,
  },
  {
    name: "Mantequilla",
    isVegan: false,
    isVegetarian: true,
  },
  {
    name: "Agua",
    isVegan: true,
    isVegetarian: true,
  },
  {
    name: "Arándanos",
    isVegan: true,
    isVegetarian: true,
  },
  {
    name: "Huevo",
    isVegan: false,
    isVegetarian: true,
  },
  {
    name: "Leche",
    isVegan: false,
    isVegetarian: true,
  },
  {
    name: "Vainilla",
    isVegan: true,
    isVegetarian: true,
  },
  {
    name: "Limón",
    isVegan: true,
    isVegetarian: true,
  },
]

const userListDocuments = userList.map((ele) => new User(ele))
const shopListDocuments = shopList.map((ele) => new Shop(ele))
const productListDocuments = productList.map((ele) => new Product(ele))
const ingredientListDocuments = ingredientList.map((ele) => new Ingredient(ele))

const applySeed = async () => {
  try {
    await User.collection.drop()
    await Shop.collection.drop()
    await Product.collection.drop()
    await Ingredient.collection.drop()
    try {
      await User.insertMany(userListDocuments)
      await Shop.insertMany(shopListDocuments)
      //await Product.insertMany(productListDocuments)
      await Ingredient.insertMany(ingredientListDocuments)

      console.log("ok!")
      mongoose.disconnect()
    } catch (err) {
      console.log("error inserting seed")
      console.log(error)
    }
  } catch (err) {
    console.log("Error emptying collections")
    console.log(err)
  }
  mongoose.disconnect()
}

applySeed()
