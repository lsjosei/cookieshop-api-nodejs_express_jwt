import mongoose from "mongoose"

const Schema = mongoose.Schema

const shopSchema = new Schema(
  {
    name: { type: String, required: true },
    address: { type: String, required: true },
    products: {},
  },
  {
    collection: "shops",
    timestamps: true,
    versionKey: false
  }
)

const Shop = mongoose.model("Shop", shopSchema)

export { Shop }
