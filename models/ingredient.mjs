import mongoose from "mongoose"

const Schema = mongoose.Schema

const ingredientSchema = new Schema(
  {
    name: { type: String, required: true },
    isVegetarian: {type: Boolean},
    isVegan: {type: Boolean},
    isGlutenFree: {type: Boolean},
    isLactoseFree: {type: Boolean}
  },
  {
    collection: "ingredients",
    timestamps: true,
    versionKey: false
  }
)

const Ingredient = mongoose.model("Ingredient", ingredientSchema)

export { Ingredient }
