import mongoose from "mongoose"

const Schema = mongoose.Schema

const productSchema = new Schema(
  {
    name: { type: String, required: true },
    price: { type: Number, required: true },
    ingredients: [{ type: mongoose.Schema.Types.ObjectId, ref:"Ingredient"}]
  },
  {
    collection: "products",
    timestamps: true,
    versionKey: false
  }
)

const Product = mongoose.model("Product", productSchema)

export { Product }
