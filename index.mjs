import express from "express"
import { connect } from "./utils/db.mjs"
import cors from "cors"

import { router as shopRoutes } from "./routes/shops.routes.mjs"
import { router as productRoutes } from "./routes/products.routes.mjs"
import { router as userRoutes } from "./routes/user.routes.mjs"
import { router as ingredientRoutes } from "./routes/ingredients.routes.mjs"

import "dotenv/config"

const PORT = process.env.PORT

//SERVER + ENRUTADOR
const server = express()
const router = express.Router()

//CORS
router.use(cors())

//JWT SECRET KEY
server.set("secretKey", "theMostUnbreakableSecretKeyEverCreated2")

//MIDDLEWARE
router.use(express.json())
server.use(express.urlencoded({ extended: true }))

//RUTAS
router.get("/", (req, res, next) => {
  return res.status(200).json({
    status: 200,
    message: "Welcome",
  })
})

server.use("/", router)
server.use("/user", userRoutes)
server.use("/shop", shopRoutes)
server.use("/product", productRoutes)
server.use("/ingredient", ingredientRoutes)


//CONTROL DE ERRORES
server.use("*", (req, res, next) => {
  const error = new Error("Route not found")
  error.status = 404
  next(error) // Lanzamos la función next() con un error
})

server.use((error, req, res, next) => {
  return res
    .status(error.status || 500)
    .json(error.message || "Unexpected error")
})

//ESCUCHA DEL SERVER
server.listen(PORT, () => {
  return console.log(`Server running at http://localhost:${PORT}`)
})
