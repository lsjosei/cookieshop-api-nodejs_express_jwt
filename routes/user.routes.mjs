import express from "express"
import cors from "cors"

import { isAuth } from "../jwt/jwt.mjs"
import { login, logout, register } from "../controllers/user.controller.mjs"

const router = express.Router()
router.use(cors())

router.post("/login", login)

router.post("/register", [isAuth], register)

router.post("/logout", logout)

export { router }
