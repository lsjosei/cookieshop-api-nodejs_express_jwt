import express from "express"
import cors from "cors"
import { isAuth } from "../jwt/jwt.mjs"
import {
  addProductToShop,
  createShop,
  deleteProductFromShop,
  deleteShop,
  getAllShops,
  getShopById,
  modifyShop,
  sellProduct,
} from "../controllers/shop.controller.mjs"

const router = express.Router()
router.use(cors())

router.get("/", getAllShops)

router.get("/:shopId", getShopById)

router.post("/create", [isAuth], createShop)

router.delete("/delete/:shopId", [isAuth], deleteShop)

router.put("/modify/:shopId", [isAuth], modifyShop)

router.patch("/addProduct/:shopId", [isAuth], addProductToShop)

router.patch("/deleteProduct/:shopId", [isAuth], deleteProductFromShop)

router.patch("/sellProduct/:shopId", [isAuth], sellProduct)

export { router }
