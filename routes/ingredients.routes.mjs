import express from "express"
import cors from "cors"
import { isAuth } from "../jwt/jwt.mjs"
import {
  createIngredient,
  deleteIngredient,
  getAllIngredients,
  getIngredientById,
  modifyIngredient,
} from "../controllers/ingredient.controller.mjs"

const router = express.Router()
router.use(cors())

router.get("/", getAllIngredients)

router.get("/:ingredientId", getIngredientById)

router.post("/create", [isAuth], createIngredient)

router.put("/modify/:ingredientId", [isAuth], modifyIngredient)

router.delete("/delete/:ingredientId", [isAuth], deleteIngredient)

export { router }
