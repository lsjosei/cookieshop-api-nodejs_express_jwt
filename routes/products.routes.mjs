import express from "express"
import { isAuth } from "../jwt/jwt.mjs"
import cors from "cors"
import {
  createProduct,
  deleteProduct,
  getAllProducts,
  getProductById,
  updateProduct,
} from "../controllers/product.controller.mjs"

const router = express.Router()
router.use(cors())

//get
router.get("/", getAllProducts)
router.get("/:productId", getProductById)

//post
router.post("/create", [isAuth], createProduct)

//delete
router.delete("/delete/:productId", [isAuth], deleteProduct)

//patch
router.patch("/update/:productId", [isAuth], updateProduct)

export { router }
